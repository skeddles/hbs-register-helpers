var glob = require("glob");
//var hbs = require('hbs');
var path = require('path');

module.exports = (hbs, pathglob,options) => {
    console.log('\nregistering helpers');

    if (!options) options = {};

    glob.sync(pathglob,options)
      //return object for each project with name and all routes
      .forEach(function (file) {
        var name = '';
        //var parentFolder = path.dirname(file).split(path.sep).pop();
        //if (parentFolder != 'helpers') name += parentFolder + '-'; // this adds the subfolder name into the helper name
        name += path.basename(file).replace(/\.js|_/g,'');                        console.log(' ','{{'+name+'}}',file);
        hbs.registerHelper(name, module.parent.require('./'+file));
      });
};